const amqp = require("amqplib/callback_api");
const errorLogs = require('./makeErrorLogs');

/**
 * Sends the received data @{data} to the given queue @{queue}.
 *
 * @param queue
 *  The queue to which to send the data to.
 * @param data
 *  The data to be sent to the aformentioned queue.
 * @pre
 *  Queue cannot be null or empty.
 *      | queue !== null || string !== ""
 * @post
 *  Message @code{data} is in queue.
 *
 */
let sendToRabbit = (queue, data) => {
    if (queue !== null || string !== ""){
        // Create connection with rabbitmq
        amqp.connect('amqp://integration_project22:3jPqsaCUBXgHHLzM@10.3.56.3:5672', function (err, connection) {
            if (err) {
                console.log("Send: Connection to rabbitMQ failed.")
                console.log(err);
                errorLogs.makeErrorXML("fatal", err);
                return;
            }

            // Create a channel for this operation
            connection.createChannel(function (err, channel) {
                if (err) {
                    console.log("Send: channel create failed.");
                    console.log(err);
                    errorLogs.makeErrorXML("fatal", err);
                    return;
                }

                //Send data to uuid_manager
                channel.assertQueue(queue, {
                    durable: false
                });
                setTimeout(function() { connection.close();}, 500);
                channel.sendToQueue(queue, Buffer.from(data));

            });
        });
    }
    else {
        console.log('Queue must not be null or empty.')
        return;
    }
};

module.exports = {
    send: sendToRabbit
};
