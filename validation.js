const Libxml = require('node-libxml');
let libxml = new Libxml();
const sender = require("./rmqSendData.js");

/**
 * Validate the incomming XML using the XSD located on the given path.
 * The sends the validated XML to the given RabbitMQ queue.
 * @param xmlTovalidate
 *  | The incomming XML to validate
 * @param pathToXSD
 *  | The path to the right XSD file
 * @param queToSendTO
 *  | The queue to which to send the XML
 * @return {*}
 * | A variable containing an error message if XSD validation was unsuccesfull,
 *  | the path to the XSD if the validation was sucessfull
 *  | and @code{null} if the XSD was not loaded in properply.
 */
let validateXML = (xmlTovalidate, pathToXSD, queToSendTO) => {

    //xsd validation of xml
    let xmlFromstring = libxml.loadXmlFromString(xmlTovalidate);
    libxml.loadSchemas([pathToXSD]);
    let testDefaultV = libxml.validateAgainstSchemas();

    // console.log(xmlTovalidate);
    // console.log(xmlFromstring);
    // console.log(pathToXSD);
    // console.log(testDefaultV);

    if (testDefaultV === null){
        if (queToSendTO !== null) {
            console.log('Validation: testDefaultV is null');
        }

        libxml.freeXml();
        libxml.freeSchemas();

        return 'The given xsd-path is incorect';
    }
    else if (testDefaultV !== pathToXSD){
        if (queToSendTO !== null) {
            console.log("Validation: error in testDefaultV: ");
            console.log(testDefaultV);
            console.log(xmlTovalidate);
            console.log(libxml.validationSchemaErrors[pathToXSD]);
        }

        libxml.freeXml();
        libxml.freeSchemas();

        return 'Someting whent wrong with the validation';
    }
    else {
        libxml.freeXml();
        libxml.freeSchemas();

        //Send xml file with conventions to rabbitMQ queue
        if (queToSendTO !== null)
            sender.send(queToSendTO, xmlTovalidate);
        else if (queToSendTO === null)
            return testDefaultV;
    }


}

module.exports = {
    validate: validateXML
}