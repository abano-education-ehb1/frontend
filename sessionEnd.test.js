

let validation = require('./validation');

const { create } = require('xmlbuilder2');
const {endSession}=require('./sessionEnd')
test("test_invoice",()=>{

        let obj = {
            request: {
                source: {
                    '#text': 'frontend'
                },
                'source-id': {
                    '#text': `456`
                },
                uuid:{

                },
                action:{
                    '#text': 'get-invoice'
                }
            }
        }

        const doc = create(obj)
        const xml2 = doc.end({prettyPrint: true});
        expect(validation.validate(xml2, './xsdFormats/factuur_xsd.xsd', null)).toBe('./xsdFormats/factuur_xsd.xsd');
    }
)
test("test_deleteSession",()=> {
    let obj = {
        session: {
            // '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            source:{
                '#text': 'frontend'
            },
            action:{
                '#text': 'delete'
            },
            'source-id':{
                '#text': `44`
            },
            uuid:{
            },
            properties: {
                name:{
                    '#text': `Event`
                },
                beginDateTime:{
                    '#text': `April 7, 2022`
                },
                endDateTime:{
                    '#text': `April 29, 2022`
                },
                speaker:{
                    '#text':`null`
                },
                location:{
                    '#text': `Brussel`
                },
                description:{
                    '#text': `description`
                }
            }
        }

    }

    const doc = create(obj);
    const xml = doc.end({prettyPrint: true});
    expect(validation.validate(xml, './xsdFormats/session_xsd.xsd', null)).toBe('./xsdFormats/session_xsd.xsd');


})
test("test_createSession",()=> {
        let obj = {
            "user-session": {
                // '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                source: {
                    '#text': 'frontend' // text node
                },
                sessionId:{
                    '#text': `478`
                },
                sessionName:{
                    '#text': `sessionname`
                },
                userId:{
                    '#text': `457`
                },
                action: {
                    '#text': `create`
                },
                email: {
                    '#text': `email@email.com`
                }
            }
        }

        const doc = create(obj);
        const xml = doc.end({prettyPrint: true});
        expect(validation.validate(xml, './xsdFormats/makeReservation_xsd.xsd', null)).toBe('./xsdFormats/makeReservation_xsd.xsd');

    }
)
test("test_updateSession",()=> {
        let obj = {
            session: {
                source:{
                    '#text': 'frontend'
                },
                action:{
                    '#text': 'update'
                },
                'source-id':{
                    '#text': `457`
                },
                uuid:{
                },
                properties: {
                    name:{
                        '#text': `event`
                    },
                    beginDateTime:{
                        '#text': `21 April, 2022`
                    },
                    endDateTime:{
                        '#text': `25 April, 2022`
                    },
                    speaker:{
                        '#text':`speaker`
                    },
                    location:{
                        '#text': `brussel`
                    },
                    description:{
                        '#text': `descrption`
                    }
                }
            }

        }

        const doc = create(obj);
        const xml = doc.end({prettyPrint: true});
        expect(validation.validate(xml, './xsdFormats/session_xsd.xsd', null)).toBe('./xsdFormats/session_xsd.xsd');

    }
)