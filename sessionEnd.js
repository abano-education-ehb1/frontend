
const { create } = require('xmlbuilder2');

const mysqlHandler = require('./mysqlHandler');
const errorLogs = require('./makeErrorLogs');
const validation = require('./validation');

/**
 * Calles the @code{}checkDate} every 9 minutes
 */
let checkEnded = async function() {
    await checkDate();
    setTimeout(checkEnded, 1000  * 60 * 9);
}
const months = {Jan: '01', Feb: '02', Mar: '03', Apr: '04', May: '05', Jun: '06', Jul: '07', Aug: '08', Sep: '09', Oct: '10', Nov: '11', Dec: '12',}
const months2 = {1: '01', 2: '02', 3: '03', 4: '04', 5: '05', 6: '06', 7: '07', 8: '08', 9: '09', 10: '10', 11: '11', 12: '12',}
/**
 * Gets the date of the day before today.
 * Because of this page -- https://stackoverflow.com/questions/31912523/how-to-get-yesterday-date-in-node-js-backend --
 *  we where not sure if @code{today.getDate() -1} would word in edge cases.
 *
 * | Sees if the current hour is 23:50 till 23:59.
 *  | If YES it sees if today is the first day of the month.
 *      | If YES it check if this month is the first month of the year
 *          | If YES it creates a date to use for getting sessions that ended the day before today accordingly.
 *          | If NOT it creates a date to use for getting sessions that ended the day before today accordingly.
 *      | If NOT it gets the date of the day before today
 *          and creates a date to use for getting sessions that ended the day before today accordingly
 * | After this it calls the @code{getNewlyEndedEvents} method for further processing.
 */
let checkDate = () => {

    const currentDate = new Date();

    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    const day = currentDate.getDate();
    const hours = currentDate.getHours();
    const minutes = currentDate.getMinutes();

    let dateToGet;
    let lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0);

    if (hours == 23 && (minutes >= 50 && minutes <= 59 )) {
        if (day == 1) {
            if (month == 1) {
                let lastYearDate = new Date(currentDate.getFullYear() - 1, 11, 0);
                dateToGet = `${lastYearDate.getFullYear()}-${lastYearDate.getMonth() + 1}-${lastYearDate.getDate()}`;
                getNewlyEndedEvents(dateToGet);
            } else {
                dateToGet = `${year}-${months2[lastDayOfMonth.getMonth()]}-${lastDayOfMonth.getDate()}`;
                getNewlyEndedEvents(dateToGet);
            }
        } else {
            dateToGet = `${year}-${months2[month]}-${day-1}`;
            getNewlyEndedEvents(dateToGet);
        }
    }

}

let instantInvoice = () => {
    const currentDate = new Date();

    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    const day = currentDate.getDate();
    const hours = currentDate.getHours();
    const minutes = currentDate.getMinutes();

    let dateToGet;
    let lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0);

    if (day == 1) {
        if (month == 1) {
            let lastYearDate = new Date(currentDate.getFullYear() - 1, 11, 0);
            dateToGet = `${lastYearDate.getFullYear()}-${lastYearDate.getMonth() + 1}-${lastYearDate.getDate()}`;
            getNewlyEndedEvents(dateToGet);
        } else {
            dateToGet = `${year}-${months2[lastDayOfMonth.getMonth()]}-${lastDayOfMonth.getDate()}`;
            getNewlyEndedEvents(dateToGet);
        }
    } else {
        dateToGet = `${year}-${months2[month]}-${day-1}`;
        getNewlyEndedEvents(dateToGet);
    }
}

/**
 * Gets the newly ended events depending on the given end date.
 * Then calls the @code{getBookings} method for further processing.
 * @param endDate
 *  | The given end date.
 */
let getNewlyEndedEvents = (endDate) => {
    let con = mysqlHandler.makeConnection();
    con.query("SELECT * FROM wp_em_events where event_end_date=?", endDate, function (err, result, fields) {
        if (err) {mysqlHandler.handleSQLError(err);return;}

        let res = JSON.parse(JSON.stringify(result));
        if (res !== undefined) {
            for (i=0; i<res.length; i++) {
                let eventId = res[i]["event_id"];
                getBookings(eventId);
            }
        }
    });
    con.end();

}

/**
 * Gets the bookings of newly ended session, so that we can get the users that where attending the session.
 * @param event_id
 *  | The id of the newly ended session.
 */
let getBookings = (event_id) => {
    let con = mysqlHandler.makeConnection();
    con.query("SELECT * FROM wp_em_bookings where event_id=? AND booking_status=1", event_id, function (err, result, fields) {
        if (err) {mysqlHandler.handleSQLError(err);return;}

        let res = JSON.parse(JSON.stringify(result));
        if (res !== undefined) {
            for (i=0; i<res.length; i++) {
                createXML(res[i]["person_id"]);
            }
        }
    });
    con.end();

}

/**
 * Creates an XML for every attended user of the newly ended session.
 * @param userid
 */
let createXML = (userid) => {
    let obj = {
        request: {
            source: {
                '#text': 'frontend'
            },
            'source-id': {
                '#text': `${userid}`
            },
            uuid:{

            },
            action:{
                '#text': 'get-invoice'
            }
        }
    }

    const doc = create(obj)
    const xml2 = doc.end({prettyPrint: true});
    validation.validate(xml2, './xsdFormats/factuur_xsd.xsd', 'uuid_manager');
}

module.exports = {
    checkEnded: checkEnded,
    instantInvoice: instantInvoice
}