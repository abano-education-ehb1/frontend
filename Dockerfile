FROM node:17

COPY package*.json /frontend/
COPY xsdFormats /frontend/xsdFormats
COPY *.js /frontend/

WORKDIR /frontend

RUN npm install
EXPOSE 80
CMD  ["node", "client.js"]