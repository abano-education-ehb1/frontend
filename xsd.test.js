let validation = require('./validation');

const { create } = require('xmlbuilder2');

test('Testing valid user-xml', () => {
    //Making mock user xml.
    let particulierXML = {
        user: {
            source:{
                '#text': 'frontend'
            },
            'source-id': {
                '#text': `sourceid`
            },
            uuid: {},
            action: {
                '#text': `create`
            },
            properties: {
                firstname:{'#text': `firstname`},
                lastname:{'#text': `lastname`},
                email: {'#text': `email`},
                address: {
                    street: {'#text': `street`},
                    housenumber: {'#text': `3`},
                    postalcode: {'#text': `2800`},
                    city: {'#text': `Mechelen`},
                    country:{'#text':`Belgium`}
                },
                phone: {'#text': `phone`}
            }
        }
    }
    //Build xml file
    const doc = create(particulierXML);
    const xml2 = doc.end({prettyPrint: true});
    console.log('----------------------------------- Testing valid user-xml -----------------------------------');
    expect(validation.validate(xml2, './xsdFormats/user_xsd.xsd', null)).toBe('./xsdFormats/user_xsd.xsd');

});

/**
 * A value that is supposed to be convertable to an integer will be set to a string that can not be casted to an integer in this xml.
 * Namely postalcode.
 * Housenumber is an integer set in string codes. This is also valid.
 */
test('Testing invalid user-xml', () => {
    //Making mock user xml.
    let particulierXML = {
        user: {
            source:{
                '#text': 'frontend'
            },
            'source-id': {
                '#text': `sourceid`
            },
            uuid: {},
            action: {
                '#text': `create`
            },
            properties: {
                firstname:{'#text': `firstname`},
                lastname:{'#text': `lastname`},
                email: {'#text': `email`},
                address: {
                    street: {'#text': `street`},
                    housenumber: {'#text': '3'},
                    postalcode: {'#text': 'string'},
                    city: {'#text': `city`},
                    country:{'#text':`country`}
                },
                phone: {'#text': `phone`}
            }
        }
    }
    //Build xml file
    const doc = create(particulierXML);
    const xml2 = doc.end({prettyPrint: true});
    console.log('----------------------------------- Testing invalid user-xml -----------------------------------');
    expect(validation.validate(xml2, './xsdFormats/user_xsd.xsd', null)).toBe('Someting whent wrong with the validation');
})

test('Testing valid company-xml', () => {
    let companyXML = {
        company: {
            source:{
                '#text': 'frontend'
            },
            'source-id': {
                '#text': `sourceid`
            },
            uuid: {},
            action: {
                '#text': `update`
            },
            properties: {
                name:{'#text': `name`},
                email: {'#text': `email`},
                address: {
                    street: {'#text': `street`},
                    housenumber: {'#text': `3`},
                    postalcode: {'#text': `2800`},
                    city: {'#text': `Mechelen`},
                    country:{'#text':`Belgium`}
                },
                phone: {'#text': `phone`},
                taxId:{'#text': `BE-2312523956`}
            }
        }
    }

    const doc = create(companyXML)
    const xml2 = doc.end({prettyPrint: true});
    console.log('----------------------------------- Testing valid company-xml -----------------------------------');
    expect(validation.validate(xml2, './xsdFormats/validate_company.xsd', null)).toBe('./xsdFormats/validate_company.xsd');

});

/**
 * A couple of the mandatory attributes, name and email, will be left out in this xml.
 */
test('Testing invalid company-xml', () => {
    let companyXML = {
        company: {
            source:{
                '#text': 'frontend'
            },
            'source-id': {
                '#text': `sourceid`
            },
            uuid: {},
            action: {
                '#text': `update`
            },
            properties: {
                address: {
                    street: {'#text': `street`},
                    housenumber: {'#text': 3},
                    postalcode: {'#text': 2800},
                    city: {'#text': `Mechelen`},
                    country:{'#text':`Belgium`}
                },
                phone: {'#text': `phone`},
                taxId:{'#text': `BE-2312523956`}
            }
        }
    }

    const doc = create(companyXML)
    const xml2 = doc.end({prettyPrint: true});
    console.log('----------------------------------- Testing invalid company-xml -----------------------------------');
    expect(validation.validate(xml2, './xsdFormats/validate_company.xsd', null)).toBe('Someting whent wrong with the validation');
})

/**
 * In this test I will give a path that leads to nothing in @code{validation.validate()}
 */
test('Testing invalid xsd path', () => {
    let companyXML = {
        company: {
            source:{
                '#text': 'frontend'
            },
            'source-id': {
                '#text': `sourceid`
            },
            uuid: {},
            action: {
                '#text': `update`
            },
            properties: {
                name:{'#text': `name`},
                email: {'#text': `email`},
                address: {
                    street: {'#text': `street`},
                    housenumber: {'#text': `3`},
                    postalcode: {'#text': `2800`},
                    city: {'#text': `Mechelen`},
                    country:{'#text':`Belgium`}
                },
                phone: {'#text': `phone`},
                taxId:{'#text': `BE-2312523956`}
            }
        }
    }

    const doc = create(companyXML)
    const xml2 = doc.end({prettyPrint: true});
    console.log('----------------------------------- Testing invalid xsd path -----------------------------------');
    expect(validation.validate(xml2, './xsdFormats/validate.xsd', null)).toBe('The given xsd-path is incorect');
})





