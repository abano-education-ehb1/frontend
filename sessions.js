const { create } = require('xmlbuilder2');
const validation = require('./validation');
const mysql = require("mysql");
const errorLogs = require('./makeErrorLogs');
const consumer = require("./rmq_receiver.js");

const mysqlHandler = require('./mysqlHandler');

let email, subject, message, userFirstName, userLastName, numOfPlaces, bookingid;

//Pipeline test
/**
 * Sees if incoming request if for a new booking or a booking cancellation.
 * And then calls the right methods for further processing.
 * @param data
 *  | The incoming request
 */
let processRegistrations = (data) => {
    email = data.to.toString();
    subject = data.subject.toString();
    message = data.message.toString();

    //converting the string into a JSON file
    let x = JSON.parse(message);

    if (subject === 'Admin Booking Confirmed')
        getPendingEvent(x);
    else if (subject === 'Admin Booking Cancelled')
        getCanceldEvent(x);

}

/**
 * Gets the booking dates into the right formating for further processing.
 * @param dates
 *  | The booking dates in string form.
 * @return {Promise<*>}
 *  | Return an array consisting of the dates in better formating.
 */
let datesToArray = async (dates) => {
    return messageArray = dates.split(" - ");
}

/**
 * Updates existing sessions depending on the incoming data.
 * @param data
 *  | The incoming data
 *  @post
 *  | The session is updated
 */
let updateSession = (data) => {
    let postId = data.post_id;
    let postMeta = data.post_meta;
    setTimeout(function () {
        let con = mysqlHandler.makeConnection();
        con.query("select * from wp_em_events where post_id=? ", postId, function (err, result) {
            if (err){
                mysqlHandler.handleSQLError(err);
                return;
            }
            let eventName, beginTime, endTime, speaker, location, description;

            let res = JSON.parse(JSON.stringify(result[0]));
            if (res !== undefined) {
                eventName = res["event_name"];

                beginTime = `${postMeta["_event_start_date"][0]}T${postMeta["_event_start_time"][0]}`;
                let bdate = new Date(beginTime).getTime()/1000;
                beginTime = bdate;
                endTime = `${postMeta["_event_end_date"][0]}T${postMeta["_event_end_time"][0]}`;
                let edate = new Date(endTime).getTime()/1000;
                endTime = edate;

                speaker = "";
                location_id = res["location_id"];
                description = res["post_content"];
                postId = res["post_id"];

                let con2 = mysqlHandler.makeConnection();
                con2.query("select location_address from wp_em_locations where location_id=? ", location_id, function (err, result2) {
                    if (err) {
                        mysqlHandler.handleSQLError(err);
                        return;
                    }
                    if (result2.toString()!==''){
                        let res2 = JSON.parse(JSON.stringify(result2[0]));

                        if (res2 !== undefined) {
                            location = res2["location_address"];
                            updateSessionXml(postId, eventName, beginTime, endTime, speaker, location, description);
                        }
                    }
                    else{
                        updateSessionXml(postId, eventName, beginTime, endTime, speaker, 'No location', description);
                    }
                });
                con2.end();

            }
        });
        con.end();
    }, 500);

}
/**
 * Deletes existing sessions depending on the incoming data.
 * @param data
 *  | The incoming data
 *  @post
 *  | The session is deleted
 */
let deleteSession = (data) => {
    let postId = data.post_id;
    let postMeta = data.post_meta;

    let eventName, beginTime, endTime, speaker, location, description;

    beginTime = `${postMeta["_event_start_date"][0]}T${postMeta["_event_start_time"][0]}`;
    let bdate = new Date(beginTime).getTime()/1000;
    beginTime = bdate;
    endTime = `${postMeta["_event_end_date"][0]}T${postMeta["_event_end_time"][0]}`;
    let edate = new Date(endTime).getTime()/1000;
    endTime = edate;


    deleteSessionXml(postId, '', beginTime, endTime, '', '', '');

}
/**
 * Processes newly created sessions, so that an XML can be sent to the other systems.
 * @param data
 *  | Data of the newly created session
 */
let processNewSession = (data) => {
    let postId = data.post_id;
    let postMeta = data.post_meta;
    setTimeout(function () {
        let con = mysqlHandler.makeConnection();
        con.query("select * from wp_em_events where post_id=?", postId, function (err, result) {
            if (err){
                handleSQLError(err);
                return;
            }
            let eventId, eventName, beginTime, endTime, speaker, location_id, location, description;

            let res = JSON.parse(JSON.stringify(result[0]));

            if (res !== undefined) {
                consumer.setInArray(postId);

                location_id = res["location_id"];

                let con2 = mysqlHandler.makeConnection();
                con2.query("select location_town, location_postcode, location_address from wp_em_locations where location_id=?", location_id, function (err, result2) {
                    if (err) {
                        mysqlHandler.handleSQLError(err);
                        return;
                    }
                    consumer.seeIfInCreateArray();
                    if (result2.toString()==''){
                        eventName = res["event_name"];

                        beginTime = `${postMeta["_event_start_date"][0]}T${postMeta["_event_start_time"][0]}`;
                        let bdate = new Date(beginTime).getTime()/1000;
                        beginTime = bdate;
                        endTime = `${postMeta["_event_end_date"][0]}T${postMeta["_event_end_time"][0]}`;
                        let edate = new Date(endTime).getTime()/1000;
                        endTime = edate;

                        speaker = "";

                        description = res["post_content"];
                        postId = res["post_id"];


                        location = 'No location';
                        createSessionXml(postId, eventName, beginTime, endTime, speaker, location, description);
                    }
                    else{
                        let res2 = JSON.parse(JSON.stringify(result2[0]));
                        if (res2 !== undefined){

                            eventName = res["event_name"];

                            beginTime = `${postMeta["_event_start_date"][0]}T${postMeta["_event_start_time"][0]}`;
                            let bdate = new Date(beginTime).getTime()/1000;
                            beginTime = bdate;
                            endTime = `${postMeta["_event_end_date"][0]}T${postMeta["_event_end_time"][0]}`;
                            let edate = new Date(endTime).getTime()/1000;
                            endTime = edate;

                            speaker = "";

                            description = res["post_content"];
                            postId = res["post_id"];


                            location = `${res2["location_town"]}, ${res2["location_postcode"]}, ${res2["location_address"]}`;
                            createSessionXml(postId, eventName, beginTime, endTime, speaker, location, description);
                        }
                    }

                });
                con2.end();
            }

        });
        con.end();
    }, 500);
}

/**
 * Makes an XML for updated sessions
 * @param postId
 *  | The session id
 * @param eventName
 *  | The session name
 * @param beginTime
 *  | The begin time of the session
 * @param endTime
 *  | The end time of the session
 * @param speaker
 *  | The speaker of session
 * @param location
 *  | The location of the event
 * @param description
 *  | The description of the session
 */
let updateSessionXml = (postId, eventName, beginTime, endTime, speaker, location, description) => {
    let obj = {
        session: {
            source:{
                '#text': 'frontend'
            },
            action:{
                '#text': 'update'
            },
            'source-id':{
                '#text': `${postId}`
            },
            uuid:{
            },
            properties: {
                name:{
                    '#text': `${eventName}`
                },
                beginDateTime:{
                    '#text': `${beginTime}`
                },
                endDateTime:{
                    '#text': `${endTime}`
                },
                speaker:{
                    '#text':`${speaker}`
                },
                location:{
                    '#text': `${location}`
                },
                description:{
                    '#text': `${description}`
                }
            }
        }

    }

    const doc = create(obj);
    const xml = doc.end({prettyPrint: true});
    validation.validate(xml, './xsdFormats/session_xsd.xsd', 'uuid_manager');
}
/**
 * Makes an XML for deleted sessions
 * @param postId
 *  | The session id
 * @param eventName
 *  | The session name
 * @param beginTime
 *  | The begin time of the session
 * @param endTime
 *  | The end time of the session
 * @param speaker
 *  | The speaker of session
 * @param location
 *  | The location of the event
 * @param description
 *  | The description of the sesion
 */
let deleteSessionXml = (postId, eventName, beginTime, endTime, speaker, location, description) => {
    let obj = {
        session: {
            // '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            source:{
                '#text': 'frontend'
            },
            action:{
                '#text': 'delete'
            },
            'source-id':{
                '#text': `${postId}`
            },
            uuid:{
            },
            properties: {
                name:{
                    '#text': `${eventName}`
                },
                beginDateTime:{
                    '#text': `${beginTime}`
                },
                endDateTime:{
                    '#text': `${endTime}`
                },
                speaker:{
                    '#text':`${speaker}`
                },
                location:{
                    '#text': `${location}`
                },
                description:{
                    '#text': `${description}`
                }
            }
        }

    }

    const doc = create(obj);
    const xml = doc.end({prettyPrint: true});
    validation.validate(xml, './xsdFormats/session_xsd.xsd', 'uuid_manager');
}
/**
 * Makes an XML for newly created sessions
 * @param postId
 *  | The session id
 * @param eventName
 *  | The session name
 * @param beginTime
 *  | The begin time of the session
 * @param endTime
 *  | The end time of the session
 * @param speaker
 *  | The speaker of session
 * @param location
 *  | The location of the event
 * @param description
 *  | The description of the session
 */
let createSessionXml = (postId, eventName, beginTime, endTime, speaker, location, description) => {
    let obj = {
        session: {
            // '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            source:{
                '#text': 'frontend'
            },
            action:{
                '#text': 'create'
            },
            'source-id':{
                '#text': `${postId}`
            },
            uuid:{
            },
            properties: {
                name:{
                    '#text': `${eventName}`
                },
                beginDateTime:{
                    '#text': `${beginTime}`
                },
                endDateTime:{
                    '#text': `${endTime}`
                },
                speaker:{
                    '#text':`${speaker}`
                },
                location:{
                    '#text': `${location}`
                },
                description:{
                    '#text': `${description}`
                }
            }
        }

    }

    const doc = create(obj);
    const xml = doc.end({prettyPrint: true});
    console.log(xml)
    validation.validate(xml, './xsdFormats/session_xsd.xsd', 'uuid_manager');
}

/**
 * Processes the incoming data of new bookings for existing sessions.
 * And calls @code{createRegistrationXML} for the XML creation.
 * @param message
 *  | The incoming data
 */
let getPendingEvent = (message) => {
    eventdates = datesToArray(message["eventdates"]).then( datesarray => {
        let eventId = message["eventID"];
        let userEmail = message["useremail"];
        eventname = message["eventname"];
        let userid;

        setTimeout(function () {
            let con = mysqlHandler.makeConnection();
            con.query("select ID from wp_users where user_email=?", userEmail, function (err, result1) {
                if (err){
                    handleSQLError(err);
                    return;
                }

                let con2 = mysqlHandler.makeConnection();
                con2.query("select * from wp_em_events where event_id=? ", eventId, function (err2, result) {
                    if (err2){
                        handleSQLError(err2);
                        return;
                    }
                    let res2 = JSON.parse(JSON.stringify(result[0]));
                    if (res2 !== undefined) {
                        postId = res2["post_id"];

                        let res = JSON.parse(JSON.stringify(result1[0]));
                        if (res !== undefined) {
                            userid = res["ID"];
                            createRegistrationXML("create", postId, userid, userEmail, eventname);
                        }
                    }
                });
                con2.end();
            });
            con.end();
        }, 500);
    });
}
/**
 * Processes the incoming data of booking cancellations for existing sessions.
 * And calls @code{createRegistrationXML} for the XML creation.
 * @param message
 *  | The incoming data
 */
let getCanceldEvent = (message) => {
    let eventId, eventname, beginTime, endTime, speaker, location, description, begindate, userFirstName, userLastName, bookingid, useremail;
    eventdates = datesToArray(message["eventdates"]).then( datesarray => {
        eventId = message["eventID"];
        eventname = message["eventname"];
        userFirstName = message["username"];
        userLastName = message["username"];
        bookingid = message["bookingid"];
        useremail = message["useremail"];


        if (datesarray.length = 2){
            begindate = datesarray[0];
            let bdate = new Date(beginTime).getTime()/1000;
            beginTime = bdate;

            enddate = datesarray[1];
            let edate = new Date(endTime).getTime()/1000;
            endTime = edate;
        }
        else{
            begindate = datesarray[0];
            enddate = datesarray[0];
        }

        let con = mysqlHandler.makeConnection();
        con.query("select ID from wp_users where user_email=?", useremail, function (err, result1) {
            if (err) {
                mysqlHandler.handleSQLError(err);
                return;
            }
            let res = JSON.parse(JSON.stringify(result1[0]));
            if (res !== undefined) {
                userid = res["ID"];
                speakers = "NOGTEDOEN";

                let con2 = mysqlHandler.makeConnection();
                con2.query("select * from wp_em_events where event_id=? ", eventId, function (err2, result) {
                    if (err2){
                        handleSQLError(err2);
                        return;
                    }
                    let res2 = JSON.parse(JSON.stringify(result[0]));
                    if (res2 !== undefined) {
                        postId = res2["post_id"];

                        let res = JSON.parse(JSON.stringify(result1[0]));
                        if (res !== undefined) {
                            userid = res["ID"];
                            createRegistrationXML("delete", postId, userid,  useremail,  eventname);
                        }
                    }
                });
                con2.end();
            }
        });
        con.end();

    });

}

/**
 * Creates an XML for booking reservations or booking cancellations, depending on the given @code{action}
 * @param action
 *  | The given action to make an XML for
 *      | Can be "Cancel-reservation" or "create"
 * @param sessionID
 *  | The session id
 * @param userID
 *  | The id of the user that is reserving or canceling
 * @param userEmail
 *  | The users email
 * @param sessionName
 * | The session name
 */
let createRegistrationXML = (action, sessionID, userID, userEmail, sessionName) => {
    let obj = {
        "user-session": {
            // '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            source: {
                '#text': 'frontend' // text node
            },
            sessionId:{
                '#text': `${sessionID}`
            },
            sessionName:{
                '#text': `${sessionName}`
            },
            userId:{
                '#text': `${userID}`
            },
            action: {
                '#text': `${action}`
            },
            email: {
                '#text': `${userEmail}`
            }
        }
    }

    const doc = create(obj);
    const xml = doc.end({prettyPrint: true});
    validation.validate(xml, './xsdFormats/makeReservation_xsd.xsd', 'uuid_manager');
}

module.exports = {
    processRegistrations: processRegistrations,
    processNewSession: processNewSession,
    updateSession:updateSession,
    deleteSession:deleteSession
}