const amqp = require("amqplib/callback_api");
const WPapi = require("wpapi");
const xml2js = require("xml2js");
let parser2 = xml2js.Parser();
const errorLogs = require('./makeErrorLogs');
const validation = require('./validation');
const {create} = require("xmlbuilder2");
const mysqlHandler = require('./mysqlHandler');

//Connection with the wordpress API
const wp = new WPapi({
    endpoint: 'http://10.3.56.3/wp-json',
    username:'frontend',
    password: '0iQb%7Ksiiv1)FI2i)'
});

/**
 * Makes the response XML that needs to be sent after a successfull entity creation, update or deletion.
 * @param res
 *  | The response given by the api with all the updated variables
 * @param uid
 *  | The uuid of the updated entity
 * @param action
 *  | The performed actio.
 *      | Can be 'create', 'update' or 'delete'
 */
let makeResponse = (sourceId, uid, action, entity) => {
    let obj = {
        response: {
            source: {
                '#text': 'frontend'
            },
            "source-id":{
                '#text': `${sourceId}`
            },
            uuid:{
                '#text': `${uid}`
            },
            entity:{
                '#text': `${entity}`
            },
            action:{
                '#text':`${action}`
            }
        }
    }

    const doc = create(obj)
    const xml2 = doc.end({prettyPrint: true});
    validation.validate(xml2, './xsdFormats/response_xsd.xsd', 'uuid_manager');
}

let source, first, email, pass, user_id, uuid
/**
 * Makes, updates or deletes users that aren't companies from the database, depending on the incomming message @code{msg}.
 * Except if the source of the message is Frontend
 * @param msg
 *  | The given message
 */
let makeUser = (msg) => {
    parser2.parseString(msg, function (err, result) {
        const json = JSON.stringify(result, null, 4);
        let x = JSON.parse(json);

        let properties = x["user"]["properties"][0];

        source = x["user"]["source"][0];
        if (source === "frontend"){return;}
        let z=properties["address"]

        first = properties["firstname"][0] + properties["lastname"][0];
        email = properties["email"][0];
        user_id = parseInt(x["user"]["source-id"]);
        uuid = x["user"]["uuid"];

        action = x["user"]["action"][0];

        if (action === "update") {
            if (seeIfInArray(user_id)) {
                removeFromArray(user_id);
            }
            setInArray(user_id);

            wp.users().id(user_id).update({
                email: email
            }).then(function (response) {

                let con = mysqlHandler.makeConnection();
                con.query("update wp_usermeta set meta_value=? where meta_key='first_name' AND user_id=? ",[properties["firstname"][0] ,user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='phone_number' AND user_id=? ",[properties["phone"][0] ,user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='last_name' AND user_id=? ",[properties["lastname"][0] ,user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='city' AND user_id=? ",[z[0]["city"][0] ,user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='postcode' AND user_id=? ",[z[0]["postalcode"][0],user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='user_housenumber' AND user_id=? ",[z[0]["housenumber"][0] ,user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='street' AND user_id=? ",[z[0]["street"][0],user_id], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("update wp_usermeta set meta_value=? where meta_key='country' AND user_id=? ",[z[0]["country"][0] ,user_id],function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.end();

                makeResponse(response["id"], uuid, "updated", "user");
            }).catch(err => {
                console.log("Receiver: WP API update error.");
                console.log(err);
                errorLogs.makeErrorXML("fatal", err);
            })
        }

        //Deleting user
        if (action === "delete"){
            setInArray(user_id);
            wp.users().id(user_id).delete({
                force: true,
                reassign: 1
            }).then(function (response) {
                makeResponse(response["id"], uuid, "deleted", "user");
            }).catch(err => {
                console.log("Receiver: WP API delete error.");
                console.log(err);
                errorLogs.makeErrorXML("fatal", err);
            })

            return;
        }

        //Creating users in wordpress with the WP-API
        if (action === 'create'){
            //Checking if password was send by the crm software if not a general password is created for the user
            if (pass==null||pass==""){
                pass="password1234"
            }
            wp.users().create({
                username: `${first}`,
                roles:"um_particulier",
                email: `${email}`,
                password: `${pass}`,

            }).then(function (response) {
                let user_id = response["id"];
                let con = mysqlHandler.makeConnection();
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"first_name",properties["firstname"][0] ], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"last_name",properties["lastname"][0] ], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"street",z[0]["street"][0]], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"city",z[0]["city"][0] ], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"country",z[0]["country"][0] ], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"user_housenumber",z[0]["housenumber"][0] ], function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"postcode",z[0]["postalcode"][0] ],function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}

                    let res = JSON.parse(JSON.stringify(result));


                });
                con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[user_id,"phone_number",properties["phone"][0] ],function (err, result, fields) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}
                    let res = JSON.parse(JSON.stringify(result));
                });
                con.end();

                setInArray(user_id);
                makeResponse(response["id"], uuid, "created", "user");
            }).catch(err => {
                console.log("Receiver: WP API error.");
                console.log(err);
                errorLogs.makeErrorXML("fatal", err);
            })

            return;
        }
    });

}

/**
 * Makes, updates or deletes users that are companies from the database, depending on the incomming message @code{msg}.
 * Except if the source of the message is Frontend
 * @param msg
 *  | The given message
 */
let makeCompany = (msg) => {
    parser2.parseString(msg, function (err, result) {
        const json = JSON.stringify(result, null, 4);
        let x = JSON.parse(json);
        let properties = x["company"]["properties"][0];

        source = x["company"]["source"][0];
        if (source === "frontend"){
            return;
        }
        let z = properties["address"];
        email = properties["email"][0];
        user_id = parseInt(x["company"]["source-id"]);
        uuid = x["company"]["uuid"][0];
        let action = x["company"]["action"][0];
           if (action === "update") {
               if (seeIfInArray(user_id)) {
                   removeFromArray(user_id);
               }
               setInArray(user_id);
               wp.users().id(user_id).update({
                   email: email
               }).then(function (response) {
                   setInArray(response["id"]);
                   makeResponse(response["id"], uuid, "updated", "company");
               }).catch(err => {
                   console.log("Receiver: WP API update error.");
                   console.log(err);
                   errorLogs.makeErrorXML("fatal", err);
               })

               let con = mysqlHandler.makeConnection();
               con.query("update wp_usermeta set meta_value=? where meta_key='BTW-nummer' AND user_id=? ", [properties["taxId"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='phone_number' AND user_id=? ", [properties["phone"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='company_name' AND user_id=? ", [properties["name"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='city' AND user_id=? ", [z[0]["city"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='postcode' AND user_id=? ", [z[0]["postalcode"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='user_housenumber' AND user_id=? ", [z[0]["housenumber"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='street' AND user_id=? ", [z[0]["street"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });
               con.query("update wp_usermeta set meta_value=? where meta_key='country' AND user_id=? ", [z[0]["country"][0], user_id], function (err, result, fields) {
                   if (err) {mysqlHandler.handleSQLError(err);return;}
                   let res = JSON.parse(JSON.stringify(result));
               });

               con.end();

               return;
           }
           //Deleting user
           else if (action === "delete"){
               setInArray(user_id);
               wp.users().id(user_id).delete({
                   force: true,
                   reassign: 1
               }).then(function (response) {
                   removeFromArray(response["ID"]);
                   makeResponse(response["id"], uuid, "deleted", "company");
               }).catch(err => {
                   console.log("Receiver: WP API delete error.");
                   console.log(err);
                   errorLogs.makeErrorXML("fatal", err);
               })
           }

           //Creating users in wordpress with the WP-API
           else if (action === 'create'){
               //Checking if password was send by the crm software if not a general password is created for the user
               if (pass==null||pass==""){
                   pass="password1234"
               }
               wp.users().create({
                   username: properties["name"][0],
                   email: `${email}`,
                   password: `${pass}`,
                   roles:"um_bedrijf"
               }).then(function (response) {
                   let new_user_id = response["id"];
                   setInArray(new_user_id);
                   let con = mysqlHandler.makeConnection();
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"BTW-nummer",properties["taxId"][0] ], function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"company_name",properties["name"][0] ], function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"street",z[0]["street"][0]], function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"city",z[0]["city"][0] ], function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"country",z[0]["country"][0] ], function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"user_housenumber",z[0]["housenumber"][0] ], function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"postcode",z[0]["postalcode"][0] ],function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });
                   con.query("insert into wp_usermeta (user_id,meta_key,meta_value) values(?,?,?)",[new_user_id,"phone_number",properties["phone"][0] ],function (err, result, fields) {
                       if (err) {mysqlHandler.handleSQLError(err);return;}
                       let res = JSON.parse(JSON.stringify(result));
                   });

                   con.end();
                   setInArray(response["id"]);
                   makeResponse(response["id"], uuid, "created", "company");

               }).catch(err => {
                   console.log("Receiver: WP API create error.");
                   console.log(err);
                   errorLogs.makeErrorXML("fatal", err);
               })
           }


    });
}

let makeSession = (msg) => {
    parser2.parseString(msg, function (err, result) {
        const json = JSON.stringify(result, null, 4);
        let x = JSON.parse(json);

        source = x["session"]["source"][0];
        if (source === "frontend") {return;}

        let properties = x["session"]["properties"][0];
        let action = x["session"]["action"][0];
        let postId = x["session"]["source-id"][0];
        let user_uuid = x["session"]["uuid"][0];

        let name = properties["name"][0];

        const months = {Jan: '01', Feb: '02', Mar: '03', Apr: '04', May: '05', Jun: '06', Jul: '07', Aug: '08', Sep: '09', Oct: '10', Nov: '11', Dec: '12',}

        let beginDateUnix = properties["beginDateTime"][0];
        let tempbeginDate = new Date(beginDateUnix * 1000);
        let beginArr = tempbeginDate.toString().split(" ");
        let beginDateTime = `${beginArr[3]}-${months[ beginArr[1] ]}-${beginArr[2]} ${beginArr[4]}`;
        let beginDate = `${beginArr[3]}-${months[ beginArr[1] ]}-${beginArr[2]}`;
        let beginTime = `${beginArr[4]}`

        let endDateUnix = properties["endDateTime"][0];
        let tempendDate = new Date(endDateUnix * 1000);
        let endArr = tempendDate.toString().split(" ");
        let endDateTime = `${endArr[3]}-${months[ beginArr[1] ]}-${endArr[2]} ${endArr[4]}`;
        let endDate = `${endArr[3]}-${months[ beginArr[1] ]}-${endArr[2]}`;
        let endTime = `${endArr[4]}`;
        let location = properties["location"][0];
        let description =  properties["description"][0];
        let con = mysqlHandler.makeConnection();

        con.end();
        if(action === 'create'){
            wp.posts().create({
                title: name,
                content: description,
                status: 'publish',
                type: 'event'
            }).then(function( response ) {
                let newPostId = response.id;
                setInCreateArray(newPostId);

                let con = mysqlHandler.makeConnection();
                con.query("UPDATE wp_posts SET post_type = ? WHERE ID = ?", ['event', newPostId], function (err, result2) {
                        if (err) {mysqlHandler.handleSQLError(err);return;}
                    let con2 = mysqlHandler.makeConnection();
                    con2.query("INSERT INTO wp_em_events (post_id, event_slug, event_status, event_owner, event_name, event_start_date, event_end_date, event_start_time, event_end_time, event_start, event_end, post_content, location_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        [newPostId, name, 1, 1, name, beginDate, endDate, beginTime, endTime, beginDateTime, endDateTime, description, 0], function (err, result2) {
                            if (err) {mysqlHandler.handleSQLError(err);return;}

                            let newEventId = result2["insertId"];

                            let con3 = mysqlHandler.makeConnection();
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_id", newEventId],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_start_local", beginDateTime],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_end_local", endDateTime],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_end_date", endDate],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_start_date", beginDate],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_end", endDateTime],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_start", beginDateTime],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_end_time", endTime],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_start_time", beginTime],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_event_rsvp", 1],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });

                            // let locarray = location.split(", ");
                            let con5 = mysqlHandler.makeConnection();
                            con5.query("INSERT INTO wp_em_locations (post_id, location_town, location_postcode, location_address) VALUES(?,?,?,?)", [newPostId, "Brussel", "1000", location],function (err, result5, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                                let locationId = result5["insertId"]
                                con3.query("INSERT INTO wp_postmeta (post_id,meta_key, meta_value) values(?,?,?)",[newPostId, "_location_id", locationId],function (err, result3, fields) {
                                    if (err) {mysqlHandler.handleSQLError(err);return;}
                                });
                                con3.query("UPDATE wp_em_events SET location_id =? WHERE event_id=?", [locationId, newEventId], function (err, result) {
                                    if (err) {mysqlHandler.handleSQLError(err);return;}
                                });
                                con3.end();
                            });
                            con5.end();


                            let con4 = mysqlHandler.makeConnection();
                            con4.query("INSERT INTO wp_em_tickets (event_id, ticket_name, ticket_price, ticket_spaces, ticket_members, ticket_required, ticket_order) VALUES (?,?,?,?,?,?,?)",
                                [newEventId, 'Standard Ticket', 0.0000, 10, 0, 0, 1], function (err, result2) {
                                if (err) {mysqlHandler.handleSQLError(err); return;}
                                    makeResponse(newPostId, user_uuid, "created", "session");
                            })
                            con4.end();
                    });
                    con2.end();
                })
                con.end();

            });
        }
        else if (action === 'delete'){
            let con = mysqlHandler.makeConnection();
            con.query("DELETE FROM wp_em_events WHERE post_id=? ", postId, function (err, result) {
                if (err) {mysqlHandler.handleSQLError(err);return;}
                let con2 = mysqlHandler.makeConnection();
                con2.query("DELETE FROM wp_posts WHERE ID=? ", postId, function (err, result) {
                    if (err) {mysqlHandler.handleSQLError(err);return;}
                    let con3 = mysqlHandler.makeConnection();
                    con3.query("DELETE FROM wp_postmeta WHERE post_id=? ", postId, function (err, result) {
                        if (err) {
                            mysqlHandler.handleSQLError(err);
                            return;
                        }
                        makeResponse(postId, user_uuid, "deleted", "session");
                    });
                    con3.end();
                });
                con2.end();
            });
            con.end();

        }
        else if (action === 'update'){
            setInArray(postId);
            let con = mysqlHandler.makeConnection();
            con.query("UPDATE wp_em_events SET event_name = ?, event_slug = ?, event_start_date = ?, event_end_date = ?, event_start_time = ?, " +
                "event_end_time = ?, event_start = ?, event_end = ?, post_content = ? WHERE post_id = ?",
                [name, name, beginDate, endDate, beginTime, endTime, beginDateTime, endDateTime, description, postId], function (err, result) {
                if (err) {mysqlHandler.handleSQLError(err); return;}
                    let con2 = mysqlHandler.makeConnection();
                    con2.query("UPDATE wp_posts SET post_title = ?, post_content = ? WHERE ID = ?",
                        [name, description, postId], function (err2, result2) {
                            if (err2) {mysqlHandler.handleSQLError(err2); return;}
                            let con3 = mysqlHandler.makeConnection();
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[beginDateTime, postId, "_event_start_local"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[endDateTime,postId, "_event_end_local"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[endDate, postId,"_event_end_date" ],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[beginDate, postId, "_event_start_date"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[endDateTime, postId, "_event_end"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[beginDateTime, postId, "_event_start"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[endTime, postId, "_event_end_time"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.query("UPDATE wp_postmeta SET meta_value=? where post_id=? AND meta_key=?",[beginTime, postId, "_event_start_time"],function (err, result3, fields) {
                                if (err) {mysqlHandler.handleSQLError(err);return;}
                            });
                            con3.end();

                            makeResponse(postId, user_uuid, "updated", "session");
                        });
                    con2.end();
            });
            con.end();
        }
    });
}


/**
 * Get messages from the queue 'frontend' from RabbitMQ and analyses them.
 * Using the XSD validation methods it is able to know if the incomming message is for particulier users of companies.
 * Depending on this it calls the right methos for further process.
 */
let consumer = () => {

    //Connection with rabbitMQ
    amqp.connect('amqp://integration_project22:3jPqsaCUBXgHHLzM@10.3.56.3:5672', function (err, connection) {
        if(err){
            console.log('Receiver: Connection to rabbitMQ failed');
            console.log(err);
            errorLogs.makeErrorXML("fatal", err);
            return;
        }
        //Create channel
        connection.createChannel(function (err, channel) {
            if (err) {
                console.log('Receiver: channel creation failed');
                console.log(err);
                errorLogs.makeErrorXML("fatal", err);
                return;
            }

            //Check if queue exists with assert
            channel.assertQueue('frontend', {
                exclusive: false,
                durable: false
            }, function (err, q) {
                if (err) {
                    console.log('Receiver: uuid_to_frontend queue creation failed');
                    console.log(err);
                    errorLogs.makeErrorXML("fatal", err);
                    return;
                }
                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);

                let first, email, pass, source;
                //Fetching the received data
                channel.consume(q.queue, function(msg) {
                    if(msg.content) {

                        msg = msg.content.toString();
                        // console.log(msg);
                        // console.log("===================================================")

                        if ( validation.validate(msg, './xsdFormats/user_xsd.xsd', null) === './xsdFormats/user_xsd.xsd' ){
                            makeUser(msg);
                        }
                        else if ( validation.validate(msg, './xsdFormats/validate_company.xsd', null) === './xsdFormats/validate_company.xsd' ){
                            makeCompany(msg);
                        }
                        else if(validation.validate(msg, './xsdFormats/session_xsd.xsd', null) == './xsdFormats/session_xsd.xsd'){
                            makeSession(msg);
                        }
                        else{
                            errorLogs.makeErrorXML("fatal", `user ${msg} could not be processed in Wordpress`);
                        }
                    }
                }, {
                    noAck: true
                });
            });
        });

    });

}

let cashArray = [];
let createArray = [];
let setInArray = (id) => {
    cashArray.push(id);
}
let removeFromArray = (id) => {
    let index = cashArray.indexOf(id);
    cashArray.pop(index);
}
let seeIfInArray = (id) => {
    return cashArray.includes(id);
}

let setInCreateArray = (id) => {
    createArray.push(id);
}
let removeFromCreateArray = (id) => {
    let index = createArray.indexOf(id);
    createArray.pop(index);
}
let seeIfInCreateArray = (id) => {
    return createArray.includes(id);
}

module.exports = {
    consumer: consumer,
    setInArray: setInArray,
    removeFromArray: removeFromArray,
    seeIfInArray: seeIfInArray,
    setInCreateArray: setInCreateArray,
    removeFromCreateArray: removeFromCreateArray,
    seeIfInCreateArray: seeIfInCreateArray
};