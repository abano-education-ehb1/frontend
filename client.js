const consumer = require("./rmq_receiver.js");
const sender = require("./rmqSendData.js");
const sessions = require('./sessions');
const validation = require('./validation');
const errorLogs = require('./makeErrorLogs');
const mysqlHandler = require('./mysqlHandler');
const endSession = require('./sessionEnd');

const express = require("express");
const bodyParser = require("body-parser");
const { create } = require('xmlbuilder2');
const xml2js = require("xml2js");
let parser2 = xml2js.Parser();


/**
 * Starts an express app and parses the received data into a JSON object.
 * Sends a status 200 after receive.
 * Listens on port 3000
 */
(
    () => {

        // Initialize express and define a port
        const app = express();
        const PORT = 3000;

        // Tell express to use body-parser's JSON parsingg
        app.use(bodyParser.json());
        app.post("/hook", (req, res) => {
            const data = req.body;
            res.status(200).end();
            setTimeout(function () {
                processData(data, 'create');
            }, 2000);
        })
        app.post("/updateUser", (req, res) => {
            const data = req.body;
            res.status(200).end();
            setTimeout(function () {
                let userId = data.ID;
                if (consumer.seeIfInArray(userId)) {
                    consumer.removeFromArray(userId);
                } else {
                    processData(data, 'update');
                }
            }, 2000);
        })
        app.post("/deleteUser", (req, res) => {
            const data = req.body;
            res.status(200).end();
            setTimeout(function () {
                let userId = data.ID;
                if (consumer.seeIfInArray(userId)) {
                    consumer.removeFromArray(userId);
                } else {
                    processData(data, 'delete');
                }
            }, 2000);
        })
        app.post("/emailhook", (req, res) => {
            const data = req.body;
            res.status(200).end();
            if (data.subject.toString() === "Invoice" )
            {
                endSession.instantInvoice();
            }
            if (data.subject.toString() === "Admin Booking Cancelled" || data.subject.toString() === "Admin Booking Pending" || data.subject.toString() === "Admin Booking Confirmed")
                sessions.processRegistrations(data);
            else
                return;
        })
        app.post("/eventcreated", (req, res) => {
            const data = req.body;
            res.status(200).end();
            let postId = data.post_id;
            setTimeout(function (){
                if (!consumer.seeIfInCreateArray(postId)) {
                    sessions.processNewSession(data);
                }
                else{
                    consumer.removeFromCreateArray(postId);
                }
            }, 2000)

        })
        app.post("/eventdeleted", (req, res) => {
            const data = req.body;
            res.status(200).end();
            sessions.deleteSession(data);
        })
        app.put("/eventupdated", (req, res) => {
            const data = req.body;
            res.status(200).end();
            setTimeout(function () {
                let postId = data.post_id;
                if(consumer.seeIfInArray(postId)){
                    consumer.removeFromArray(postId);
                    return;
                }
                else if(consumer.seeIfInCreateArray(postId)) {
                    consumer.removeFromCreateArray(postId);
                    return;
                }
                else{
                    sessions.updateSession(data);
                }
            }, 2000);
        })

        // Start express on the defined port
        app.listen(PORT, () => console.log(`🚀 Server running on port ${PORT}`))
    }
)();

/**
 * Makes XML for deleted users
 * @param body
 *  | The body of the request sent by the webhook
 * @param action
 *  | The action for which we are makin this xml. In this case it should be 'delete'
 */
let makeDeletedXML = (body, action) => {
    let email = body.user.data.user_email;
    let userLogin = body.user.data.user_login;
    let role = body.user.roles[0];
    let userId = body.user.ID;

    publishMessage(userId, 'none', 'none', role, email, 'none', 'none', 0, 'none', 'none', 'none', 'none', 0, action);
}

/**
 * Processes the given request body and calls other methods depending on the given @code{action}
 * It first takes the usable data from the webhook.
 * Then it gets the remaining data that can not be found in the webhook from the database.
 * After that it calls @code{publishMessage()} with the needed data in the arguments.
 * @param body
 *  | The body of the request sent by the webhook
 * @param action
 *  | The action for which we are making this xml.
 *  | the @code{action} can be @code{create}, @code{update} or @code{delete}
 */
let processData = (body, action) => {

    if (action === 'delete'){
        makeDeletedXML(body, action);
        return;
    }

    //Variables for data from webhook
    let email = body.data.user_email;
    let userLogin = body.data.user_login;
    let role = body.roles[0];
    let userId = body.ID;

    //Variables for data from wp db
    let phone, houseNumber, street, country, postcode, city, firstname, lastname, BTW, company;
    setTimeout(function () {

        /*
         * Verbinding maken met de wordpress database.
         */
        let con = mysqlHandler.makeConnection();

        /*
           * Hier data krijgen van wordpress database
           * aan de hand van de verkregen ID van de user.
           * */
        con.query("select meta_value from wp_usermeta where user_id=? " +
            "and (meta_key='first_name' " +
            "OR meta_key= 'last_name' " +
            "OR meta_key= 'phone_number' " +
            "OR meta_key= 'postcode' " +
            "OR meta_key= 'user_housenumber' " +
            "OR meta_key= 'city' " +
            "OR meta_key= 'company_name' " +
            "OR meta_key= 'BTW-nummer' " +
            "OR meta_key= 'country' " +
            "OR meta_key= 'street'" + ")", userId, function (err, result, fields) {
            if (err) {mysqlHandler.handleSQLError(err);return;}

            let res = JSON.parse(JSON.stringify(result));
            /* Steekt de verkregen data in variabelen.
             * Dit afhangend van of de nieuwe gebruiker een particulier is of een bedrijf.
            * */
            if (res !== undefined) {

                if (res[2] === undefined){
                    console.log('Client: User does not have all metadata')
                    return;
                }

                consumer.setInArray(userId);

                if (role === 'um_particulier') {
                    firstname = res[0].meta_value;
                    lastname = res[1].meta_value;
                }
                if (role === 'um_bedrijf') {
                    BTW = res[4].meta_value;
                    company = res[3].meta_value;
                }

                country = res[2].meta_value;
                street = res[5].meta_value;
                houseNumber = parseInt(res[6].meta_value.toString());
                postcode = parseInt(res[7].meta_value.toString());
                city = res[8].meta_value;
                phone = res[9].meta_value;

            }

            publishMessage(userId, firstname, lastname, role, email, country, street, houseNumber, phone, BTW, company, city, postcode, action);

        });
        con.end();
    }, 500);

}

/**
 * Makes an XML using the given data. Then calles the validation method for XSD validation.
 * @param firstName
 * @param lastName
 * @param role
 * @param email
 * @param country
 * @param street
 * @param housenumber
 * @param phone
 * @param company
 * @param city
 * @param postcode
 *
 * @pre
 * | Params cannot be null or empty
 * @returns @code{number}
 *
 * @post
 * | user_data was sent to Queue
 */
function publishMessage(userId, firstName, lastName, role, email, country, street, housenumber, phone, BTW, company, city, postcode, action) {

    if(email==null || country==null  || street==null || housenumber==null || phone==null || city==null || postcode==null)
    {
        console.log("Client: General values cannot be empty");
        errorLogs.makeErrorXML("fatal", "Client data not fully in database.");
        return;
    }

    let obj;

    //Maken van xml file met XML2JS
    if (role === 'um_bedrijf') {
        obj = {
            company: {
                source:{
                    '#text': 'frontend'
                },
                'source-id': {
                    '#text': `${userId}`
                },
                uuid: {},
                action: {
                    '#text': `${action}`
                },
                properties: {
                    name:{'#text': `${company}`},
                    email: {'#text': `${email}`},
                    address: {
                        street: {'#text': `${street}`},
                        housenumber: {'#text': `${housenumber}`},
                        postalcode: {'#text': `${postcode}`},
                        city: {'#text': `${city}`},
                        country:{'#text':`${country}`}
                    },
                    phone: {'#text': `${phone}`},
                    taxId:{'#text': `${BTW}`}
                }
            }
        };

        //Build xml file
        const doc = create(obj)
        const xml2 = doc.end({prettyPrint: true});
        validation.validate(xml2, './xsdFormats/validate_company.xsd', 'uuid_manager');

    }


    if (role === 'um_particulier') {
        obj = {
            user: {
                source:{
                    '#text': 'frontend'
                },
                'source-id': {
                    '#text': `${userId}`
                },
                uuid: {},
                action: {
                    '#text': `${action}`
                },
                properties: {
                    firstname:{'#text': `${firstName}`},
                    lastname:{'#text': `${lastName}`},
                    email: {'#text': `${email}`},
                    address: {
                        street: {'#text': `${street}`},
                        housenumber: {'#text': `${housenumber}`},
                        postalcode: {'#text': `${postcode}`},
                        city: {'#text': `${city}`},
                        country:{'#text':`${country}`}
                    },
                    phone: {'#text': `${phone}`}
                }
            }
        };


        //Build xml file
        const doc = create(obj)
        const xml2 = doc.end({prettyPrint: true});
        validation.validate(xml2, './xsdFormats/user_xsd.xsd', 'uuid_manager');

    }

    return 0;
}


/**
 * Sends a heartbeat every 3 seconds to RabbitMQ on the queue 'monitoring'.
 * The heartbeat is the XML object @{xml}
 */
let heartbeat = () => {

    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let unixTimestamp = Math.floor(new Date(`${currentDate} ${timestamp}`).getTime()/1000);

    let obj =
        {
            heartbeat: {
                source: {
                    '#text': 'frontend'
                },
                date: {
                    '#text': `${unixTimestamp}`
                }
            }
        };

    const doc = create(obj)
    const xml = doc.end({prettyPrint: true});

    validation.validate(xml, './xsdFormats/validate_heartbeats.xsd', 'monitoring')
}
setInterval(heartbeat, 3000);

consumer.consumer();
endSession.checkEnded();
