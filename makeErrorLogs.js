const { create } = require('xmlbuilder2');
const validation = require('./validation');

/**
 * Makes an error log XML for every error that needs to be sent to Monitoring.
 * Also calls validation method for XSD validation.
 * @param level
 *  | The severity level of the error
 *  | Can ben 'fatal', 'error', 'warn'
 * @param message
 *  | The message of the occuring error
 */
let makeErrorXML = (level, message) => {

    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let unixTimestamp = Math.floor(new Date(`${currentDate} ${timestamp}`).getTime()/1000);

    let obj = {
        error:{
            source:{
                '#text': `frontend`
            },
            date:{
                '#text': `${unixTimestamp}`
            },
            level:{
                '#text': `${level}`
            },
            message:{
                '#text': `${message}`
            }
        }
    }

    const doc = create(obj);
    const xml = doc.end({prettyPrint: true});

    validation.validate(xml, './xsdFormats/errorlog_xsd.xsd', 'errors');

}

module.exports = {
    makeErrorXML: makeErrorXML
}