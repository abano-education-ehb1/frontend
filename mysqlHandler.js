const mysql = require("mysql");
const errorLogs = require('./makeErrorLogs');

/**
 * Handles SQL errors. This by firs printing the error to the console and then calling the
 * error handling method so that the error is also sent to Monitoring.
 * @param err
 *  | The occuring error
 */
let handleSQLError = (err) => {
    console.log('Client: WP database error.')
    console.log(err);
    if (err.fatal === true)
        errorLogs.makeErrorXML("fatal", err.sqlMessage);
    else
        errorLogs.makeErrorXML("error", err.sqlMessage);
}

/**
 * Handles database connection errors. This by firs printing the error to the console and then calling the
 * error handling method so that the error is also sent to Monitoring.
 * @param err
 *  | The occuring error
 */
let handleDBConnectionError = (err) => {
    console.log('Client: WP database connection error.')
    console.log(err);

    if (err.fatal === true)
        errorLogs.makeErrorXML("fatal", err.sqlMessage);
    else
        errorLogs.makeErrorXML("error", err.sqlMessage);
}

/**
 * Makes a connection to the MySQL database and calls the @code{handleDBConnectionError} method if needed.
 * @return {con}
 *  | A connention variable to the MySQL database
 */
let makeConnection = () => {
    let con = mysql.createConnection({
        host: "10.3.56.3",
        port: "3306",
        user: "frontend",
        database: "frontend",
        password: "LuG5JCJJNP7xDhr3cfGwv58ybe4Mnhde"
    });
    con.connect(function (err) {
        if (err){
            handleDBConnectionError(err);
            return;
        };
    });

    return con;
}

module.exports = {
    makeConnection: makeConnection,
    handleSQLError: handleSQLError
}